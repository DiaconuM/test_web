<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Admin page</title>
</head>
<body>

<?php require_once "connect.php" ?>


<div class="container">

    <?php
    $mysqli = new mysqli('localhost', 'root', '', 'test_web') or die(mysqli_error($mysqli));

    $result = $mysqli->query('SELECT * FROM products') or die($mysqli->error);
    ?>
    <div class="row justify-content-center">
        <table class="table">
            <thead>
            <tr>
                <th> title</th>
                <th> text</th>
                <th> category</th>
                <th> image</th>
                <th colspan="2"> Action</th>

            </tr>
            </thead>

            <?php while ($row = $result->fetch_assoc()): ?>
                <tr>
                    <td> <?php echo $row['title']; ?></td>
                    <td> <?php echo $row['text']; ?></td>
                    <td> <?php echo $row['category']; ?></td>
                    <td>

                        <img src="<?php echo $row['image'] ?>">
                    </td>
                    <td>

                        <a href="admin.php?edit= <?php echo $row['id']; ?>"
                           class="btn btn-info">Edit</a>

                        <a href="connect.php?delete=<?php echo $row['id']; ?>"
                           class="btn btn-danger">Delete</a>

                    </td>
                </tr>
            <?php endwhile; ?>

        </table>
    </div>

    <div class="row justify-content-center">

        <form action="connect.php" method="post" enctype=multipart/form-data>

            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" class="form-control" placeholder="Enter your name">
            </div>
            <div class="form-group">
                <label>Text</label>
                <input type="text" name="text" class="form-control"
                       placeholder="Enter your text">
            </div>
            <div class="form-group">
                <label>Category</label>
                <input type="text" name="category" class="form-control" placeholder="Enter your category">
            </div>
            <div class="form-group">
                <label>image</label>

                <input type="file" name="fileToUpload" id="fileToUpload">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="save">Save</button>
            </div>
        </form>


    </div>

</div>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>




</body>
</html>