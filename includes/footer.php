<div class='container-fluid'>
    <div class='row'>
        <div class='container'>
            <div class='row'>
                <div class='footer-section'>
                    <div class="col-md-8">
                        <a href="/">
                            <img src="images/offline.png" alt="Test Logo" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-6">
                            <p>Info</p>
                            <h5>
                                <a href="dummy.html">Application</a>
                            </h5>
                            <h5><a href="dummy.html">Products</a></h5>
                            <h5><a href="dummy.html">About us</a></h5>
                        </div>
                        <div class="col-md-6 col-md-push-1">
                            <p>Offices</p>
                            <h5><a href="dummy.html">Netherlands</a></h5>
                            <h5><a href="dummy.html">Dubai</a></h5>
                        </div>
                    </div>
                </div>
            </div>

            <hr id="footer-line">

            <div class="row">
                <div class="copyright-section">
                    <div class="col-md-8">
                        <p><a href="/">(c) Test Web 2018</a></p>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-6">
                            <p><a href="#">Terms and conditions</a></p>
                        </div>
                        <div class="col-md-6 col-md-push-2">
                            <p><a href="#">Privacy policy</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





</body>
</html>