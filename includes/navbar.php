<div class='container-fluid'>
    <div class='row'>
        <header>
            <div class='container'>
                <div class='row'>
                    <nav id='cssmenu'>
                        <div class='logo'>
                            <a href='/'>
                                <img src='images/offline.png' alt="Test logo" class="desktop-logo"/>
                            </a>
                        </div>
                        <div class='button'></div>
                        <ul>
                            <li class="desktop-menu active"><a href='#'>overview</a></li>
                            <li class="desktop-menu"><a href='#'>applications</a></li>
                            <li class="desktop-menu"><a href='#'>products</a></li>
                            <li class="desktop-menu desktop-menu-last"><a href='#'>about us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    </div>
</div>