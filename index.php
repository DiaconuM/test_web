<?php include('includes/header.php');
include('includes/navbar.php');
require_once 'connect.php';
?>



<div class='container-fluid'>
    <div class='row'>
        <div id='myCarousel' class='carousel slide myCarousel' data-interval='false' data-ride='carousel'>
            <ol class='carousel-indicators'>
                <li data-target='#myCarousel' data-slide-to='0' class='active'></li>
                <li data-target='#myCarousel' data-slide-to='1'></li>
                <li data-target='#myCarousel' data-slide-to='2'></li>
            </ol>
            <div class="progress-bar-wrapper">
                <hr class='transition-timer-carousel-progress-bar' />
            </div>
            <!-- Carousel items -->
            <div class='carousel-inner'>
                <div class='active item'>
                    <!-- Astronaut image from first slide -->
                    <img src='uploads/astronaut.jpg' alt='astronaut' class="img-responsive" />
                    <div class='container'>
                        <div class='carousel-caption'>
                            <div class="container">
                                <div class='carousel-caption-table'>
                                    <div class='carousel-caption-table-cell'>
                                        <h3>A lot has changed<br/> last 50 years</h3>
                                        <p>We went to the moon which triggered our fantasy and<br/> inspiration during the 'space age'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='item'>
                    <!-- Image from second slide -->
                    <img src='uploads/astronaut.jpg' alt='astronaut' />
                    <div class='container'>
                        <div class='carousel-caption'>
                            <div class="container">
                                <div class='carousel-caption-table'>
                                    <div class='carousel-caption-table-cell'>
                                        <h3>A lot has changed<br/> last 50 years</h3>
                                        <p>We went to the moon which triggered our fantasy and<br/> inspiration during the 'space age'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='item'>
                    <!-- Image from third slide -->
                    <img src='uploads/astronaut.jpg' alt='astronaut' />
                    <div class='container'>
                        <div class='carousel-caption'>
                            <div class="container">
                                <div class='carousel-caption-table'>
                                    <div class='carousel-caption-table-cell'>
                                        <h3>A lot has changed<br/> last 50 years</h3>
                                        <p>We went to the moon which triggered our fantasy and<br/> inspiration during the 'space age'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='container-fluid full-width-desc'>
    <div class='row'>
        <div class='container'>
            <h2>We bring smart technology to help<br/> you build a sustainable business in a<br/> healthy world</h2>
            <p>Our mission is clear: Use technology to keep the world a great place to live. We do that by developing smart applications that read huge amounts of data from sensornetworks and use analytics and machine learning to transform it into actionable information.</p>
        </div>
    </div>
</div>






<div class='container-fluid three-row-container'>

    <!-- Aici completeaza HTML -->

    <div class="row card">
        <div class="col-md-4">
            <div class="card text-center">
                <img src="uploads/240_1.jpg" class="card-img-top">
                <h6 class="text-left">MILESTONE</h6>
                <div class="card-body">
                    <h5 class="card-title">
                        <b>
                            <a href="#" class="change-text" target="_blank">Taleb school system  tracking 8000 students</a>
                        </b>
                    </h5>
                    <p class="card-text">In hac habitasse platea dictumst. Vivamus adlpiscing farmentum quam volutpat allquam.... </p>
                </div>


            </div>
        </div>
        <div class="col-md-4">
            <div class="card text-center">
                <img src="uploads/240_2.jpg" class="card-img-top">
                <h6 class="text-left">MILESTONE</h6>
                <div class="card-body">
                    <h5 class="card-title">
                        <b>
                            <a href="#" class="change-text" target="_blank">Alcochem applying vision technology to count flies demo</a>
                        </b>
                    </h5>
                    <p class="card-text">Curabitur lobortis Id lorem Id bibendum. Ut Id consecteur magna. Qulsque volutpat...</p>
                </div>


            </div>
        </div>
        <div class="col-md-4">
            <div class="card text-center">
                <img src="uploads/240_3.jpg" class="card-img-top">
                <h6 class="text-left">REFLOW LABS</h6>
                <div class="card-body">
                    <h5 class="card-title">
                        <b>
                            <a href="#" class="change-text" target="_blank">Indoor location services for wayfinding hospital</a>
                        </b>
                    </h5>
                    <p class="card-text">Vestibulum rutrum quam vitae fringilla ticidunt. Suspendisse nec tortor urna. Ut lauret sod... </p>
                </div>
            </div>
        </div>



    </div>

</div>

<div class='container-fluid'>
    <!-- Aici completeaza HTML -->
    <div class="row">
        <nav class="navbar navbar-primary bg-primary">
                       <span class="navbar-text">
                           Check how we can kickstart your smart projects
                               <span class="glyphicon">&#xe092;</span>
                       </span>
        </nav>
    </div>
</div>

<div class='container-fluid case-section-container'>
    <div class='row'>
        <div class='container'>
            <div class='row'>
                <div class='case-section'>
                    <div class="col-md-12">
                        <h5><a href='#'>CASE</a></h5>
                        <h4><a href="#">Nam porttitor blandit accumsan. Ut vel dictum.</a></h4>
                        <p>Curabitur lobortis id lorem id bibendum. Ut id consectetur magna.
                            Quisque volutpat augue enim, pulvinar lobortis nibh lacinia at.
                            Vestibulum nec erat ut mi sollicitudin porttitor id sit amet risus.
                            Nam tempus vel odio vitae aliquam. In imperdiet eros.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='container-fluid'>
    <div class='row'>
        <div class="meet-team-background">
            <div class='container'>
                <div class='meet-team-section'>
                    <div class="highlight-upper-line "></div>
                    <h3>MEET THE TEAM</h3>
                    <div class="slick-slider">
                        <div>
                            <div class="slide-div">
                                <img class="img-responsive" src="uploads/488x488.png"/>
                                <h5>Daniel Jameson</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Ut pretium pretium tempor. Ut eget imperdiet neque.
                                    In volutpat ante.</p>
                            </div>
                        </div>
                        <div>
                            <div class="slide-div">
                                <img class="img-responsive" src="uploads/488x488.png"/>
                                <h5>Donald Garrett</h5>
                                <p>Donec facilisis tortor ut augue lacinia, at viverra est semper.
                                    Sed sapien metus, scelerisque nec pharetra id, tempor a torto.</p>
                            </div>
                        </div>
                        <div>
                            <div class="slide-div">
                                <img class="img-responsive" src="uploads/488x488.png"/>
                                <h5>Ryan Howell</h5>
                                <p>Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui.
                                    In malesuada enim in dolor euismod, id commodo mi consectet.</p>
                            </div>
                        </div>
                        <div>
                            <div class="slide-div">
                                <img class="img-responsive" src="uploads/488x488.png"/>
                                <h5>slide-4</h5>
                                <p>Donec facilisis tortor ut augue lacinia, at viverra est semper.
                                    Sed sapien metus, scelerisque nec pharetra id, tempor a torto.</p>
                            </div>
                        </div>
                        <div>
                            <div class="slide-div">
                                <img class="img-responsive" src="uploads/488x488.png"/>
                                <h5>Cosmin</h5>
                                <p>Nam porttitor blandit accumsan. Ut vel dictum sem, a pretium dui.
                                    In malesuada enim in dolor euismod, id commodo mi consectet.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='container-fluid'>
    <!-- Aici completeaza HTML -->
    <div class="row">
        <nav class="navbar navbar-primary bg-primary">
                       <span class="navbar-text">
                           Check how we can kickstart your smart projects
                               <span class="glyphicon">&#xe092;</span>
                       </span>
        </nav>
    </div>
</div>

<?php
include ('includes/footer.php');
include ('includes/scripts.php'); ?>





